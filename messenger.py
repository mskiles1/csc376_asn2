"""
CSC 376 Distributed Systems
Assignment 2 - Messenger
Michael Skiles
"""

import threading
import sys
import os
import socket
import getopt

def usage( script_name ):
    print( 'Usage: python3 ' + script_name + '[-l] [host address] <port number>' )

argv = sys.argv
argc = len( sys.argv )
if argc < 2 or argc > 4 :
    usage( sys.argv[0] )
    os.exit(1)

def parse_opts( argv, argc ):
    run_as_server = False
    port = ''
    host = 'localhost'

    options, args = getopt.getopt(argv[1:], "l")
    try:
        opt, val = options[0]
        if opt == '-l':
            run_as_server = True
    except IndexError:
        pass

    try:
        port = args[0]

        # make sure the port argument is an int, throws ValueError
        int(port)
    except (IndexError, ValueError):
        print("The port number was either not specified, or not an integer")
        sys.exit()

    try:
        host = args[1]
    except IndexError:
        pass

    return [run_as_server, port, host]


class Messenger( threading.Thread ):
    """Messenger class used by both Client and Server.
       Supports asynchronous reading and writing.
    """
    def __init__( self, socket ):
        super().__init__()
        self.sock = socket
        
    def run( self ):
        message = self.sock.recv( 4096 )
        while message:
            print( message.decode(), end='' )
            message = self.sock.recv( 4096 )
        self.sock.close()
        os._exit(0)

    def get_input( self ):
        for line in sys.stdin:
            self.sock.send( line.encode() )
        self.sock.close()
        os._exit(0)

class Server:
    sock = None

    def __init__( self, port ):
        self.port = port
        self.get_connection( self.port )

    def get_connection( self, port ):
        serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        serversocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        serversocket.bind( ('localhost', int(port)) )
        serversocket.listen(5)
        self.sock, addr = serversocket.accept()
#        print("Connection accepted")

    def run_messenger( self ):
        messenger = Messenger( self.sock )
        messenger.start()
        messenger.get_input()
        messenger.join()
        
    def clean_up( self ):
        self.sock.close()

class Client:
    sock = None

    def __init__( self, port, host ):
        self.host = host
        self.port = port
        self.start_connection( self.port )

    def start_connection( self, port ):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect( (self.host, int(self.port)) )
        if self.sock is None:
            print("Could not open socket.")
#        else:
#            print("socket opened on ", str(self.sock.getsockname()))

    def run_messenger( self ):
        messenger = Messenger( self.sock )
        messenger.start()
        messenger.get_input()
        messenger.join()
        
    def clean_up( self ):
        self.sock.close()
        print("Client socket closed.")
        os._exit(0)


def main():
    args = parse_opts( argv, argc )
    run_as_server = args[0]
    port = args[1]
    host = args[2]
    if run_as_server is True:
        serv = Server( port )
        serv.run_messenger()
#        serv.clean_up()

    else:
        client = Client( port, host )
        client.run_messenger()
#        client.clean_up()

    print("Done in main")

main()
